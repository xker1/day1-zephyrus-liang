O:
&ensp;&ensp;(1) I played a team ice breaking game and expressed my expectations during my time at BootCamp through sticky notes.
&ensp;&ensp;(2) Understand the composition, function, and how to draw a concept map, and work with team members to complete the "What is computer The design of a conceptual diagram with a focus.
&ensp;&ensp;(3) Understood the role and form of the station meeting, and conducted the first station meeting.
&ensp;&ensp;(4) Learn the concept of ORID and use it to conduct daily summary reviews.
R: During the process of collaborating with team members to complete the concept map, I felt the power of team collaboration, and I was very happy with the collision of ideas with team members.
I: This activity has brought team members closer together and increased team activity.
D: You can use ORID to summarize what you learn every day and deepen your impression.